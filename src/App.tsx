import React from 'react';
import HomePage from './view/pages/Home';

function App() {
  return <HomePage />;
}

export default App;
