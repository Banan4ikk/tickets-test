import {rootState} from '../store';

export const selectCheckboxState = (state: rootState) => state.checkboxes;
