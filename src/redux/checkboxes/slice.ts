import {createSlice, PayloadAction} from '@reduxjs/toolkit';

const initialState: boolean[] = [] as boolean[];

const checkboxSlice = createSlice({
  name: 'checkboxes',
  initialState,
  reducers: {
    initState(state, {payload}: PayloadAction<{count: number}>) {
      if (payload) {
        state = new Array<boolean>(payload.count).fill(false);
      }
      return state;
    },
    updateState(state, {payload}: PayloadAction<{index: number}>) {
      if (payload) {
        let oldValue = state.find((item, index) => index === payload.index);
        state[payload.index] = !oldValue;
      }
      return state;
    },
    fillTrue(state) {
      state.fill(true);
      return state;
    },
    resetState(state) {
      state.fill(false);
      return state;
    },
  },
});

const actions = {...checkboxSlice.actions};
const reducer = checkboxSlice.reducer;

export {actions, reducer};
