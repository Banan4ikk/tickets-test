export type CheckboxItem = {
  checked: boolean;
  label: string;
};
