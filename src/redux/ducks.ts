import {combineReducers} from 'redux';
import * as checkboxes from './checkboxes';
import * as tickets from './tickets';
import * as filters from './filters';

export const rootReducer = combineReducers({
  checkboxes: checkboxes.reducer,
  tickets: tickets.reducer,
  filters: filters.reducer,
});

export const actions = {
  checkboxes: checkboxes.actions,
  tickets: tickets.actions,
  filters: filters.actions,
};

export const selectors = {
  checkboxes: checkboxes.selectors,
  tickets: tickets.selectors,
  filters: filters.selectors,
};
