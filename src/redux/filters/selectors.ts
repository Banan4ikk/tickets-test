import {rootState} from '../store';

export const selectFilters = (state: rootState) => state.filters;
