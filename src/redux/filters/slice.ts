import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {CheckboxLabelItem} from './types';

const initialState: CheckboxLabelItem[] = [] as CheckboxLabelItem[];

const filtersSlice = createSlice({
  name: 'filters',
  initialState,
  reducers: {
    addItem(state, {payload}: PayloadAction<CheckboxLabelItem>) {
      if (payload) {
        state.push(payload);
      }
      return state;
    },
    removeItem(state, {payload}: PayloadAction<CheckboxLabelItem>) {
      if (payload) {
        const index = state.findIndex(item => item === payload);
        if (index) {
          state.splice(index, 1);
        }
      }
      return state;
    },
  },
});

const actions = {...filtersSlice.actions};
const reducer = filtersSlice.reducer;

export {actions, reducer};
