import {rootState} from '../store';
import {createSelector} from '@reduxjs/toolkit';

export const selectTickets = (state: rootState) => state.tickets;

export const selectSortedTickets = () =>
  createSelector(selectTickets, state => {
    const tickets = [...state];
    return tickets.sort((item_1, item_2) => {
      return item_1.price - item_2.price;
    });
  });
