import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {TTicketInfo} from './types';
import {CheckboxLabelItem} from '../filters/types';

const initialState: TTicketInfo[] = [] as TTicketInfo[];

const ticketsSlice = createSlice({
  name: 'tickets',
  initialState,
  reducers: {
    initState(state, {payload}: PayloadAction<TTicketInfo[]>) {
      return payload;
    },
  },
});

const actions = {...ticketsSlice.actions};
const reducer = ticketsSlice.reducer;

export {actions, reducer};
