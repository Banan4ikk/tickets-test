import React, {FC} from 'react';
import {
  CheckboxContainer,
  HiddenCheckbox,
  Icon,
  StyledCheckbox,
} from './styles';
import {TCheckboxProps} from './types';
import {checkboxesLabels} from '../../../pages/Home/data';
import {actions} from '../../../../redux/ducks';
import {useDispatch} from 'react-redux';

const Checkbox: FC<TCheckboxProps> = ({checked, index}) => {
  const dispatch = useDispatch();

  const changeCheckbox = (index: number) => {
    dispatch(actions.checkboxes.updateState({index}));
  };

  const changeFilters = () => {
    if (checked) {
      dispatch(actions.filters.removeItem(checkboxesLabels[index]));
    } else {
      dispatch(actions.filters.addItem(checkboxesLabels[index]));
    }
  };

  const onAllClick = () => {
    if (checked) {
      dispatch(actions.checkboxes.resetState());
    } else {
      dispatch(actions.checkboxes.fillTrue());
    }
  };

  return (
    <CheckboxContainer>
      <HiddenCheckbox
        checked={checked}
        onChange={() => {
          changeCheckbox(index);
          if (checkboxesLabels[index].stops === -1) {
            onAllClick();
          }
          changeFilters();
        }}
      />
      <StyledCheckbox checked={checked}>
        {checked ? (
          <Icon viewBox="0 0 26 26">
            <polyline points="20 6 9 17 4 12" />
          </Icon>
        ) : null}
      </StyledCheckbox>
    </CheckboxContainer>
  );
};

export default Checkbox;
