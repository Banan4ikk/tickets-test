import styled from 'styled-components';

type Props = {
  checked: boolean;
};

export const CheckboxContainer = styled.div`
  display: inline-block;
  vertical-align: middle;
`;

export const Icon = styled.svg`
  fill: none;
  stroke: #4ba6e8;
  stroke-width: 2px;
  width: 21px;
  height: 21px;
`;

export const HiddenCheckbox = styled.input.attrs({type: 'checkbox'})`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

export const StyledCheckbox = styled.div<Props>`
  display: inline-block;
  width: 22px;
  height: 22px;
  background: #fff;
  border-radius: 7px;
  transition: all 150ms;
  margin-right: 7px;
  border: ${props =>
    props.checked ? '1px solid #7bbcef' : '1px solid #7A7A7AFF'};
`;
