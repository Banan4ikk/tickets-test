export type TCheckboxProps = {
  checked: boolean;
  index: number;
};
