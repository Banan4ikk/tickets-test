import React, {FC, memo} from 'react';
import {getNoun} from '../../../../utils/getNoun';
import {
  AirCompanyInfo,
  AirlineInfo,
  AirportInfo,
  AirportsInfoContainer,
  AirportText,
  ButtonText,
  DateText,
  FlightInfo,
  HorizontalDivider,
  PlaneContainer,
  PriceButton,
  StopsContainer,
  StopsText,
  TicketContainer,
  Time,
  TimeInfo,
} from './styles';
import {TTicketInfo} from './types';

const TicketsInfo: FC<TTicketInfo> = props => {
  return (
    <TicketContainer>
      <AirlineInfo>
        <AirCompanyInfo>{props.carrier}</AirCompanyInfo>
        <PriceButton>
          <ButtonText>Купить</ButtonText>
          <ButtonText>за {props.price} ₽</ButtonText>
        </PriceButton>
      </AirlineInfo>
      <FlightInfo>
        <TimeInfo>
          <Time>{props.departure_time}</Time>
          <StopsContainer>
            <StopsText>
              {props.stops}
              {props.stops === 0
                ? ' Пересадок'
                : getNoun(
                    props.stops,
                    ' Пересадка',
                    ' Пересадки',
                    ' Пересадки',
                  )}
            </StopsText>
            <PlaneContainer>
              <HorizontalDivider />
              <img src="/img/svg/plane.svg" alt="plane" />
            </PlaneContainer>
          </StopsContainer>
          <Time>{props.arrival_time}</Time>
        </TimeInfo>
        <AirportsInfoContainer>
          <AirportInfo>
            <AirportText>
              {props.origin}, {props.origin_name}
            </AirportText>
            <DateText>{props.departure_date}</DateText>
          </AirportInfo>
          <AirportInfo>
            <AirportText>
              {props.destination_name}, {props.destination}
            </AirportText>
            <DateText>{props.arrival_date}</DateText>
          </AirportInfo>
        </AirportsInfoContainer>
      </FlightInfo>
    </TicketContainer>
  );
};

export default TicketsInfo;
