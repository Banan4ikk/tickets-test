import styled from 'styled-components';

export const TicketContainer = styled.div`
  background: #fff;
  border-radius: 10px;
  box-shadow: 0 5px 10px 2px rgba(34, 60, 80, 0.2);
  display: flex;
  flex-direction: row;
  margin-bottom: 20px;
`;

export const AirlineInfo = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-width: 170px;
  border-right: 2px solid #dedede;
`;

export const AirCompanyInfo = styled.p`
  font-weight: 600;
  font-size: 36px;
  font-family: Arial, sans-serif;
  margin-bottom: 5px;
`;

export const PriceButton = styled.div`
  padding: 10px;
  width: 100%;
  border-radius: 7px;
  background: #ff6d00;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  transition: all 0.1s ease-in-out;

  &:hover {
    cursor: pointer;
    background: #ff8124;
  }

  &:active {
    transform: translateY(1px);
    background: #e15e01;
  }
`;

export const ButtonText = styled.p`
  font-family: Arial, sans-serif;
  font-weight: 600;
  color: #fff;
  text-wrap: normal;
  margin-bottom: 5px;
`;

export const FlightInfo = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const TimeInfo = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const Time = styled.span`
  font-family: Arial, sans-serif;
  color: #3a3a3a;
  font-size: 36px;
  margin: 0 5px;
`;

export const StopsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const PlaneContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const HorizontalDivider = styled.div`
  background: #dedede;
  height: 2px;
  width: 200px;
`;

export const StopsText = styled.p`
  color: #b4b4b4;
  font-weight: 600;
  font-family: Arial, sans-serif;
  text-transform: uppercase;
  font-size: 14px;
`;

export const AirportsInfoContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  margin-top: 10px;
`;

export const AirportInfo = styled.div`
  display: flex;
  flex-direction: column;
`;

export const AirportText = styled.p`
  font-family: Arial, sans-serif;
  font-weight: 600;
  font-size: 14px;
  color: #3a3a3a;
  letter-spacing: 0.4px;
`;

export const DateText = styled.p`
  font-family: Arial, sans-serif;
  font-size: 14px;
  color: #b4b4b4;
`;
