import React, {FC} from 'react';
import {FiltersContainer, Title} from './styles';
import MoneyButtons from '../moneyButtons';
import {buttonsArray} from '../moneyButtons/data';
import PlanePlaces from '../places';

const Filters: FC = () => {
  return (
    <FiltersContainer>
      <Title>Валюта</Title>
      <MoneyButtons items={buttonsArray} />
      <Title>Количество пересадок</Title>
      <PlanePlaces />
    </FiltersContainer>
  );
};

export default Filters;
