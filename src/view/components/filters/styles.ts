import styled from 'styled-components';

export const FiltersContainer = styled.div`
  padding: 15px;
  background: #fff;
  box-shadow: 0 5px 10px 2px rgba(34, 60, 80, 0.2);
  border-radius: 5px;
  overflow: hidden;
  max-height: 400px;
`;

export const Title = styled.div`
  text-transform: uppercase;
  font-family: Arial, sans-serif;
  color: #717172;
  margin-bottom: 10px;
  font-weight: 600;
  font-size: 14px;
`;
