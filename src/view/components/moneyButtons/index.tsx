import React, {FC, useState} from 'react';
import {Container, StyledItem, StyledText} from './styles';
import {TButtonsProps} from './types';

const MoneyButtons: FC<TButtonsProps> = ({items}) => {
  const [activeIndex, setActiveIndex] = useState(0);

  return (
    <Container>
      {items.map((item, index) =>
        index === activeIndex ? (
          <StyledItem isActive key={index}>
            <StyledText isActive>{item}</StyledText>
          </StyledItem>
        ) : (
          <StyledItem
            isActive={false}
            onClick={() => setActiveIndex(index)}
            key={index}>
            <StyledText isActive={false}>{item}</StyledText>
          </StyledItem>
        ),
      )}
    </Container>
  );
};

export default MoneyButtons;
