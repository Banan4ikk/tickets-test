import styled from 'styled-components';

type Props = {
  isActive: boolean;
};

export const Container = styled.div`
  width: auto;
  height: 50px;
  margin-bottom: 30px;
  border-radius: 6px;
  overflow: hidden;
  display: flex;
  background-color: #fff;
  justify-content: center;
`;

export const StyledItem = styled.div<Props>`
  width: 100px;
  height: 100%;
  background-color: ${props => (props.isActive ? '#2196f3' : '#fff')};
  display: flex;
  justify-content: center;
  align-items: center;
  border: ${props => !props.isActive && '1px solid #e1e0e0'};
  &:hover {
    cursor: pointer;
    background: ${props => !props.isActive && '#f2fcff'};
    border: ${props => !props.isActive && '1px solid #cfe8fb'};
  }
`;

export const StyledText = styled.span<Props>`
  font-weight: 600;
  font-family: Arial, sans-serif;
  color: ${props => (props.isActive ? '#fff' : '#41a6f5')};
  font-size: 16px;
`;
