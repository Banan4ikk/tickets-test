import React, {FC, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {CheckboxesContainer, CheckboxItem, CheckboxTitle} from './styles';
import {checkboxesLabels} from '../../pages/Home/data';
import Checkbox from '../UI/Checkbox';
import {actions, selectors} from '../../../redux/ducks';

const PlanePlaces: FC = () => {
  const dispatch = useDispatch();
  const checkboxesState = useSelector(selectors.checkboxes.selectCheckboxState);

  useEffect(() => {
    if (checkboxesState === []) {
      dispatch(actions.checkboxes.initState({count: checkboxesLabels.length}));
    }
  }, []);

  return (
    <CheckboxesContainer>
      {checkboxesState.map((item, index) => (
        <CheckboxItem key={index}>
          <CheckboxTitle>
            <Checkbox checked={item} index={index} />
            {checkboxesLabels[index].label}
          </CheckboxTitle>
        </CheckboxItem>
      ))}
    </CheckboxesContainer>
  );
};

export default PlanePlaces;
