import styled from 'styled-components';

export const CheckboxesContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 10px;
`;

export const CheckboxItem = styled.div`
  width: 115%;
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 10px;
  transform: translateX(-20px);
  padding: 5px 20px;
  transition: background 0.15s ease-in-out;

  &:hover {
    cursor: pointer;
    background: #f1fcff;
  }
`;

export const CheckboxTitle = styled.label`
  display: flex;
  align-items: center;
  font-family: Arial, sans-serif;
`;
