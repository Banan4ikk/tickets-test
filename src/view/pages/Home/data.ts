export const checkboxesLabels = [
  {label: 'Все', stops: -1},
  {label: 'Без пересадок', stops: 0},
  {label: '1 пересадка', stops: 1},
  {label: '2 пересадки', stops: 2},
  {label: '3 пересадки', stops: 3},
];
