import React, {useEffect, useMemo, useState} from 'react';
import {
  DataContainer,
  Header,
  HeaderIcon,
  TicketsContainer,
  Wrapper,
} from './styles';
import Filters from '../../components/filters';
import TicketsInfo from '../../components/UI/TicketInfo';
import {useDispatch, useSelector} from 'react-redux';
import {actions, selectors} from '../../../redux/ducks';
import data from '../../../data/tickets.json';
import {TTicketInfo} from '../../../redux/tickets/types';
import {checkboxesLabels} from './data';

const HomePage = () => {
  const dispatch = useDispatch();
  const ticketsRedux = useSelector(selectors.tickets.selectSortedTickets());
  const filters = useSelector(selectors.filters.selectFilters);
  const [filteredTickets, setFilteredTickets] = useState(ticketsRedux);

  const filterTickets = () => {
    ticketsRedux.reduce((newArr, item) => {
      filters.map(
        checkbox => checkbox.stops === item.stops && newArr.push(item),
      );
      setFilteredTickets(newArr);
      return newArr;
    }, [] as TTicketInfo[]);
  };

  useEffect(() => {
    dispatch(actions.tickets.initState(data.tickets));
    dispatch(actions.checkboxes.initState({count: checkboxesLabels.length}));
  }, []);

  useEffect(() => {
    filterTickets();
  }, [filters]);

  const memoTickets = useMemo(
    () =>
      filters.length === 0 || filteredTickets.length === 0
        ? ticketsRedux.map((item, index) => (
            <TicketsInfo {...item} key={index} />
          ))
        : filteredTickets.map((item, index) => (
            <TicketsInfo {...item} key={index} />
          )),
    [filteredTickets, ticketsRedux],
  );

  return (
    <Wrapper>
      <Header>
        <HeaderIcon src="/img/png/plane-header.png" alt="plane" />
      </Header>
      <DataContainer>
        <Filters />
        <TicketsContainer>{memoTickets}</TicketsContainer>
      </DataContainer>
    </Wrapper>
  );
};

export default HomePage;
