import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  background: #f3f7fa;
`;

export const Header = styled.div`
  width: 100%;
  height: 300px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const HeaderIcon = styled.img`
  width: 150px;
  height: 150px;
`;

export const DataContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: center;
`;

export const TicketsContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 30px;
`;
